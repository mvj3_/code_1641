//
//  Car.h
//  Learning0507
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
//@class创建一个前向引用，告诉编译器,这是一个类，其他的暂时不用管
//使用着这种方式，而不是直接import，可以防止重复编译
@class Engine;  
@class Tire;

@interface Car : NSObject
{
    Engine * engine;
    Tire * tires[4];
}
-(void)print;

-(Engine *)engine;
-(void)setEngine:(Engine *)engine;

-(Tire *)tireAtIndex:(int) index;
-(void)setTire:(Tire *)tire
       atIndex: (int)index;

@end
