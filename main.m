//
//  main.m
//  Learning0507
//
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Tire.h"
#import "Engine.h"

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        Car *car = [[Car alloc] init];
        NSLog(@"1. car初始化时car的retainCount : %ld", (unsigned long)[car retainCount]);
        NSLog(@"1. car初始化时engine的retainCount : %ld\n\n", (unsigned long)[[car engine] retainCount]);
        
        int i;
        for(i = 0 ; i < 4; i ++ ){
            Tire *tire;
            tire = [[Tire alloc] init];
            NSLog(@"2循环：tire初始化时的retainCount:%ld", (unsigned long)[tire retainCount]);
            
            [tire setPressure: 23 + i];
            [tire setTreadDepth: 33 - i];
            
            [car setTire:tire atIndex:i];
            NSLog(@"2循环：car setTire后，tire的retainCount:%ld", (unsigned long)[tire retainCount]);
            
            [tire release];
            NSLog(@"2循环：tire release后，tire的retainCount:%ld", (unsigned long)[tire retainCount]);
            NSLog(@"2循环：循环一次结束时，car的retainCount:%ld\n\n", (unsigned long)[car retainCount]);
        }
        
        Engine *engine = [[Engine alloc] init];
        NSLog(@"3. engine初始化时engine(外)的retainCount:%ld", (unsigned long)[engine retainCount]);
        [car setEngine:engine];
        NSLog(@"3. car setEngine后engine(外)的retainCount:%ld", (unsigned long)[engine retainCount]);
        NSLog(@"3. car setEngine后engine(car)的retainCount:%ld", (unsigned long)[[car engine] retainCount]);
        [engine release];
        NSLog(@"3. engine release后engine(外)的retainCount:%ld", (unsigned long)[engine retainCount]);
        NSLog(@"3. engine release后engine(car)的retainCount:%ld\n\n", (unsigned long)[[car engine] retainCount]);
        
        [car print];
        [car release];
        NSLog(@"3. car release后engine(外)的retainCount:%ld", (unsigned long)[engine retainCount]);
    }
    return 0;
}