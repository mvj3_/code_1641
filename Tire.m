//
//  Tire.m
//  Learning0507
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Tire.h"

@implementation Tire

-(id)init{
    if(self = [super init]){
        pressure = 34.0;
        treadDepth = 20.0;
    }
    return self;
}

-(void) setPressure:(float)pressureValue{
    pressure = pressureValue;
}
-(float) pressure{
    return pressure;
}

-(void) setTreadDepth:(float)treadDepthValue{
    treadDepth = treadDepthValue;
}
-(float) treadDepth{
    return treadDepth;
}

-(NSString *)description{
    NSString * desc = [NSString stringWithFormat:@"Tire: Pressure: %.1f; TreadDepth: %.1f", pressure, treadDepth, nil];
    return desc;
}
@end
