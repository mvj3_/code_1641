//
//  Tire.h
//  Learning0507
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tire : NSObject
{
    float pressure; //轮胎压力
    float treadDepth; //胎面花纹深度
}

-(void) setPressure:(float)pressure;
-(float) pressure;

-(void) setTreadDepth:(float)treadDepth;
-(float) treadDepth;

@end
