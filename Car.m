//
//  Car.m
//  Learning0507
//
//  Created by  apple on 13-5-7.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Car.h"
#import "Tire.h"
#import "Engine.h"

@implementation Car

-(id)init{
    if(self = [super init]){
        engine = [[Engine alloc] init];

        tires[0] = [Tire new];
        tires[1] = [Tire new];
        tires[2] = [Tire new];
        tires[3] = [Tire new];
    }
    return self;
}

-(Engine *)engine{
    return engine;
}

-(void)setEngine:(Engine *)engineValue{
    [engineValue retain];
    NSLog(@"    3.1. setEngine release前engine(car)的retainCount : %ld", (unsigned long)[engine retainCount]);
    [engine release];
    NSLog(@"    3.1. setEngine release后engine(car)的retainCount : %ld", (unsigned long)[engine retainCount]);
    engine = engineValue;
    NSLog(@"    3.1. engine = engineValue后engine(car)的retainCount : %ld", (unsigned long)[engine retainCount]);
}


-(void)print{  
    NSLog(@"4. %@", engine);
    
    NSLog(@"4. %@", tires[0]);
    NSLog(@"4. %@", tires[1]);
    NSLog(@"4. %@", tires[2]);
    NSLog(@"4. %@\n\n", tires[3]);
}

-(Tire *)tireAtIndex:(int) index{
    if(index < 0 || index > 3){
        NSLog(@"bad index %d in tireAtIndex", index);
        exit(1);
    }
    return tires[index];
}

-(void)setTire:(Tire *)tire atIndex: (int)index{
    if(index < 0 || index > 3){
        NSLog(@"bad index %d in tireAtIndex", index);
        exit(1);
    }
    [tire retain];
    [tires[index] release];
    tires[index] = tire;
}

//- (void)dealloc
//{
//    [engine release];
//    [tires[0] release];
//    [tires[1] release];
//    [tires[2] release];
//    [tires[3] release];
//
//    [super dealloc];
//}

@end
